from breezypythongui import EasyFrame
import timeit
import serial
import tkFileDialog

class BreezyDawnDemo(EasyFrame):

    def __init__(self):
        """Sets up the window and the widgets."""
        EasyFrame.__init__(self, title = "Breezy Dawn Demo")
        self.currentResponseTime = 0
        self.responseTimes = None
        self.responseTimeStrings = None
        self.endTime = 0
        self.startTime = 0
        self.currentResponseTimeString = "Response time: 0.0s"
        self.firstRun = True
        self.fileContents = None
        self.currentWordIndex = 0
        self.deviceInitialized = False
        self.fileNameOpen = ""

        try:
            print("doing init")
            # open serial port
            self.ser = serial.Serial("/dev/ttyUSB1", 115200, timeout=100)
            print(self.ser.readline())
            self.deviceInitialized = True
        except:
            print ("couldn't initialize device")

        self.startPanel = self.addPanel(row = 0, column = 0)
        self.connectDeviceButton = self.startPanel.addButton(text = "Connect Device", row = 0, column = 0, columnspan = 2, command = self.devInit)
        self.connectDeviceStatusLabel = self.startPanel.addLabel(text = "", row = 1, column = 0)
        if (self.deviceInitialized != True):
            self.connectDeviceButton["state"] = "disabled"
            self.connectDeviceStatusLabel["text"] = "Couldn't initialize device"
        self.openWordFileButton = self.startPanel.addButton(text = "Open Word File", row = 2, column = 0, columnspan = 2, command = self.onOpen)
        self.openWordFileStatusLabel = self.startPanel.addLabel(text = "", row = 3, column = 0)
        self.startEvalButton = self.startPanel.addButton(text = "Start Evaluation", row = 4, column = 0, columnspan = 2, state = "disabled", command = self.startEvaluation)

    def resetDevice(self):
        try:
            self.ser.timeout = 100
            self.ser.write("R")
            print(self.ser.readline())
            print(self.ser.readline())
            self.ser.flushInput()
            self.ser.flushOutput()
            self.ser.timeout = 0
        except:
            print("failed device reset")

    def onOpen(self):
        ftypes = [('Text files', '*.txt'), ('All files', '*')]
        dlg = tkFileDialog.Open(self, filetypes = ftypes)
        fl = dlg.show()

        if fl != '':
            # Get the file name on a system with a Linux-like path
            fileName = fl.rpartition("/")
            if (fileName[0] == ""):
                # Try to get the file name on a system with a Windows-like path
                fileName = fl.rpartition('\\')
            if (fileName[0] == ""):
                # If neither of the above work, just fallback to the full file path
                self.fileNameOpen = fl
            else:
                self.fileNameOpen = fileName[2]
            self.fileContents = self.readFile(fl)

            self.responseTimes = [None] * len(self.fileContents)
            self.responseTimeStrings = [None] * len(self.fileContents)
            if self.fileContents != None:
                self.openWordFileButton["state"] = "disabled"
                self.openWordFileStatusLabel["text"] = "Words from '"+self.fileNameOpen+"' added"
                if (self.connectDeviceButton["state"] == "disabled"):
                    self.startEvalButton["state"] = "active"

    def readFile(self, filename):
        try:
            f = open(filename, "r")
            lines = [line.rstrip('\n') for line in f]
            return lines
        except:
            print ("error opening word file")
            self.openWordFileStatusLabel["text"] = "Error opening word file"
            return None

    def devInit(self):
        self.ser.write("A")
        print(self.ser.readline())
        print(self.ser.readline())
        self.ser.timeout = 0
        self.connectDeviceButton["state"] = "disabled"
        self.connectDeviceStatusLabel["text"] = "Connected"
        if (self.openWordFileButton["state"] == "disabled"):
            self.startEvalButton["state"] = "active"

    def startEvaluation(self):
        self.startPanel.grid_remove()
        if (self.firstRun):
            self.evaluationPanel = self.addPanel(row = 0, column = 0)
            self.wordLabel = self.evaluationPanel.addLabel(text = self.fileContents[self.currentWordIndex], row = 0, column = 0)
            self.evaluationPanel.addLabel(text = "", row = 1, column = 0)
            self.evaluationPanel.addLabel(text = "", row = 2, column = 0)
            self.evaluationPanel.addLabel(text = "", row = 3, column = 0)
            self.leftButton = self.evaluationPanel.addButton(text = "Left", row = 4, column = 0, command = lambda: self.stopTimeShowResult("Left"))
            self.rightButton = self.evaluationPanel.addButton(text = "Right", row = 4, column = 1, command = lambda: self.stopTimeShowResult("Right"))
        else:
            # In this case this is not the first evaluation and the evaluationPanel is just hidden, so unhide it
            self.evaluationPanel.grid()
            self.wordLabel["text"] = self.fileContents[self.currentWordIndex]

        # Read the time for the start comparison
        self.startTime = timeit.default_timer()
        self.currentResponseTime = 0
        self.endTime = 0
        self.checkAliveLoop()

    def nextWord(self):
        self.resetDevice()

        self.firstRun = False
        self.currentResultsPanel.grid_remove()

        self.leftButton.grid()
        self.rightButton.grid()
        self.wordLabel["text"] = self.fileContents[self.currentWordIndex]

        # Read the time for the start comparison
        self.startTime = timeit.default_timer()
        self.currentResponseTime = 0
        self.endTime = 0
        self.checkAliveLoop()

    def stopTimeShowResult(self, direction, endTime=None):
        # If the Arduino buttons were pushed, the endTime will already be recorded, if not then capture now
        if (endTime == None):
            self.endTime = timeit.default_timer()
        # Calculate the interval
        self.currentResponseTime = self.endTime - self.startTime
        self.responseTimeStrings[self.currentWordIndex] = "{:.9f}".format(self.currentResponseTime)
        self.currentResponseTimeString = "Response time: "+self.responseTimeStrings[self.currentWordIndex]+"s"
        self.responseTimes[self.currentWordIndex] = self.currentResponseTime
        self.currentWordIndex += 1

        if (self.firstRun):
            self.currentResultsPanel = self.evaluationPanel.addPanel(row = 2, column = 0)
            self.responseDirectionLabel = self.currentResultsPanel.addLabel(text="Pushed: "+direction, row = 0, column = 0)
            self.timeDisplayLabel = self.currentResultsPanel.addLabel(text=self.currentResponseTimeString, row = 1, column = 0)
            self.nextButton = self.currentResultsPanel.addButton(text = "Next", row = 2, column = 0, command = self.nextWord)
        else:
            # In this case this is not the first evaluation, so update the results labels and unhide the results panel
            self.responseDirectionLabel.config(text="Pushed: "+direction)
            self.timeDisplayLabel.config(text = "Response time: "+"{:.9f}".format(self.currentResponseTime)+"s")
            self.currentResultsPanel.grid()

        if (self.currentWordIndex >= len(self.fileContents)):
            self.nextButton["text"] = "Show All Results"
            self.nextButton["command"]= self.showAllResults

        # Once a selection has been made, the evaluation input buttons are hidden
        self.leftButton.grid_remove()
        self.rightButton.grid_remove()

    def showAllResults(self):
        self.firstRun = False
        self.currentResultsPanel.grid_remove()
        self.evaluationPanel.grid_remove()

        self.allResultsPanel = self.addPanel(row = 0, column = 0)
        self.allResultsPanel.addLabel(text="Test Words", row = 0, column = 0)
        self.allResultsPanel.addLabel(text="Response Time", row = 0, column = 1)
        for i in range(len(self.fileContents)):
            self.allResultsPanel.addLabel(text=self.fileContents[i], row = i+1, column = 0)
            self.allResultsPanel.addLabel(text=self.responseTimeStrings[i]+"s", row = i+1, column = 1)
        self.allResultsPanel.addButton(text = "Restart", row = len(self.fileContents)+1, column = 0, command = self.restartFromBeginning)

    def restartFromBeginning(self):
        self.resetDevice()
        self.currentWordIndex = 0

        self.firstRun = False
        self.leftButton.grid()
        self.rightButton.grid()
        self.allResultsPanel.grid_remove()
        self.nextButton["text"] = "Next"
        self.nextButton["command"]= self.nextWord
        self.openWordFileButton["state"] = "active"
        self.startPanel.grid()

    # This function is written to execute every 1 ms as soon as the evaluation has started and not stop looping until a choice has been selected
    def checkAliveLoop(self):
        # If endTime is zero then the evaluation hasn't been completed yet
        if (self.endTime == 0):
            # This check-alive functionality is currently disabled for testing of 1 ms loops

            #elapsedTime = timeit.default_timer() - self.startTime
            # This elapsedTime check is in place to make sure an alert isn't generated on the first call
            #if (elapsedTime > 2):
                #print("Hello?")
                # It is possible to create a popup window like the following:
                #self.messageBox(title = "Alert", message = "Are you still here?")
            # This line causes the function to call itself again in 2 seconds
            #BDD.after(2000, self.checkAliveLoop)

            # inWaiting tells us if the Arduino has data to report (a button press)
            serialReplyWaiting = self.ser.inWaiting()
            # We are looking for a signal of "BUTTON1" or "BUTTON2" so wait for more than 6 characters
            if (serialReplyWaiting > 6):
              serialReply = self.ser.readline()
              print("Got serial reply, loop completed. Reply is:")
              print(serialReply)
              self.endTime = timeit.default_timer()
              self.stopTimeShowResult(serialReply, self.endTime)
            else:
              # Keep checking every ms
              BDD.after(1, self.checkAliveLoop)
        else:
            print("Completed")

BDD = BreezyDawnDemo()

#Instantiate and pop up the window.
BDD.mainloop()
